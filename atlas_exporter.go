package main

import (
	"net/http"
	"net/url"
	"path"
	"strconv"
	"sync"
	"time"

	"encoding/json"
	"flag"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/common/log"
	"os"
	"strings"
)

const (
	namespace = "ripe_atlas_probe"
)

// Metric represents a metric gathered from probe properties
type Metric struct {
	Name, Help string
	Type       prometheus.ValueType
	GetValue   func(p *ProbeProperties) float64
}

func (m *Metric) desc(labels []string) *prometheus.Desc {
	return prometheus.NewDesc(namespace+"_"+m.Name, m.Help, labels, nil)
}

func (m *Metric) metric(p *ProbeProperties) prometheus.Metric {
	l := []string{"id"}
	v := []string{strconv.Itoa(p.ID)}
	return prometheus.MustNewConstMetric(m.desc(l), m.Type, m.GetValue(p), v...)
}

func (m *Metric) metricValue(p *ProbeProperties, val float64) prometheus.Metric {
	l := []string{"id"}
	v := []string{strconv.Itoa(p.ID)}
	return prometheus.MustNewConstMetric(m.desc(l), m.Type, val, v...)
}

// Exporter represents the metric exporter configuration
type Exporter struct {
	URLs   []string
	mutex  sync.RWMutex
	client http.Client

	totalScrapes prometheus.Counter
	up           Metric
	metrics      []Metric
}

// NewExporter creates a metric exporter with the given options
func NewExporter(ids []string, apiURL string, timeout time.Duration) (*Exporter, error) {
	u, _ := url.Parse(apiURL)
	basePath := u.Path
	for i, id := range ids {
		u.Path = path.Join(basePath, id)
		ids[i] = u.String()
	}
	log.Info(ids)

	return &Exporter{
		URLs:   ids,
		client: http.Client{Timeout: timeout},
		totalScrapes: prometheus.NewCounter(prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "exporter_total_scrapes",
			Help:      "Current total Ripe Atlas probe scrapes.",
		}),
		up: Metric{
			Name: "up",
			Help: "Was the last scrape of the Ripe Atlas probe successful.",
			Type: prometheus.GaugeValue,
			GetValue: func(p *ProbeProperties) float64 {
				return 0
			},
		},
		metrics: []Metric{
			{
				Name: "connected",
				Help: "Current probe connection",
				Type: prometheus.GaugeValue,
				GetValue: func(p *ProbeProperties) float64 {
					c := 0
					if p.Status.Name == "Connected" {
						c = 1
					}
					return float64(c)
				},
			},
			{
				Name: "last_connected",
				Help: "Unix timestamp of the last probe connection",
				Type: prometheus.GaugeValue,
				GetValue: func(p *ProbeProperties) float64 {
					return float64(p.LastConnected.Unix())
				},
			},
			{
				Name: "uptime_seconds_total",
				Help: "Total uptime in seconds",
				Type: prometheus.CounterValue,
				GetValue: func(p *ProbeProperties) float64 {
					return float64(p.LastConnected.Unix())
				},
			},
			{
				Name: "uptime_percent",
				Help: "Total uptime percent",
				Type: prometheus.GaugeValue,
				GetValue: func(p *ProbeProperties) float64 {
					t := float64(p.LastConnected.Sub(p.FirstConnected.Time))
					return float64(p.TotalUptime.Duration) / t
				},
			},
		},
	}, nil
}

// Describe describes all the metrics ever exported by the Ripe Atlas API exporter.
// It implements prometheus.Collector.
func (e *Exporter) Describe(ch chan<- *prometheus.Desc) {
	ch <- e.up.desc(nil)
	ch <- e.totalScrapes.Desc()
}

// Collect fetches the stats from configured Ripe Atlas API and delivers them
// as Prometheus metrics. It implements prometheus.Collector.
func (e *Exporter) Collect(ch chan<- prometheus.Metric) {
	e.mutex.Lock()
	defer e.mutex.Unlock()

	// Get metrics for all probes
	for _, u := range e.URLs {
		p, up := e.scrape(u)
		if p == nil {
			continue
		}
		ch <- e.up.metricValue(p, up)
		for _, m := range e.metrics {
			ch <- m.metric(p)
		}
	}

	e.totalScrapes.Inc()
	ch <- e.totalScrapes
}

// scrape retrieves the data for a probe
func (e *Exporter) scrape(url string) (p *ProbeProperties, up float64) {
	resp, err := http.Get(url)
	if err != nil {
		log.Errorf("Can't scrape API: %v", err)
		return
	}

	p = new(ProbeProperties)
	err = json.NewDecoder(resp.Body).Decode(p)
	if err != nil {
		p = nil
		log.Errorf("Can't decode JSON: %v", err)
		return
	}

	up = 1
	return
}

func main() {
	var apiURL, probes, listenAddress, metricsPath string

	flag.StringVar(&listenAddress, "web.listen-address", ":9701", "Address to listen on for web interface and telemetry.")
	flag.StringVar(&metricsPath, "web.telemetry-path", "/metrics", "Path under which to expose metrics")
	flag.StringVar(&apiURL, "probe.api-url", "https://atlas.ripe.net/api/v2/probes/", "URL to the Ripe Atlas Probe API")
	flag.StringVar(&probes, "probe.ids", "", "Probe IDs separated by ,")
	flag.Parse()

	ids := strings.Split(probes, ",")
	if len(ids) == 0 || len(ids[0]) == 0 {
		log.Error("No probes configured.")
		os.Exit(1)
	}

	e, _ := NewExporter(ids, apiURL, 5*time.Second)

	prometheus.MustRegister(e)

	log.Infoln("Listening on", listenAddress)
	http.Handle(metricsPath, promhttp.Handler())
	log.Fatal(http.ListenAndServe(listenAddress, nil))
}

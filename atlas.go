package main

import (
	"strconv"
	"time"
)

// UnixTime represents a Unix timestamp in JSON
type UnixTime struct {
	time.Time
}

// DurationSec represents a duration in seconds in JSON
type DurationSec struct {
	time.Duration
}

// UnmarshalJSON converts a JSON Unix timestamp to a UnixTime
func (t *UnixTime) UnmarshalJSON(b []byte) error {
	ts, err := strconv.Atoi(string(b))
	if err != nil {
		return err
	}

	t.Time = time.Unix(int64(ts), 0)

	return nil
}

// UnmarshalJSON converts a timestamp in seconds to a DurationSec.
func (t *DurationSec) UnmarshalJSON(b []byte) error {
	ts, err := strconv.Atoi(string(b))
	if err != nil {
		return err
	}

	t.Duration = time.Duration(ts) * time.Second

	return nil
}

// ProbeStatus represents the current probe status
type ProbeStatus struct {
	Since time.Time
	ID    int
	Name  string
}

// ProbeTag represents a probe tag
type ProbeTag struct {
	Name, Slug string
}

// ProbeGeometry represents probe geo information
type ProbeGeometry struct {
	Type        string
	Coordinates [2]float32
}

// ProbeProperties represents the properties of a Ripe Atlas Probe
type ProbeProperties struct {
	ID          int         `json:"id"`
	Type        string      `json:"type"`
	Description string      `json:"description"`
	Status      ProbeStatus `json:"status"`
	StatusSince UnixTime    `json:"status_since"`
	Geometry    ProbeGeometry

	FirstConnected UnixTime    `json:"first_connected"`
	LastConnected  UnixTime    `json:"last_connected"`
	TotalUptime    DurationSec `json:"total_uptime"`
	CountryCode    string      `json:"country_code"`
	IsPublic       bool        `json:"is_public"`
	IsAnchor       bool        `json:"is_anchor"`

	AddressV4 string `json:"address_v4"`
	AddressV6 string `json:"address_v6"`
	PrefixV4  string `json:"prefix_v4"`
	PrefixV6  string `json:"prefix_v6"`
	ASNV4     int    `json:"asn_v4"`
	ASNV6     int    `json:"asn_v6"`

	Tags []ProbeTag `json:"tags"`
}
